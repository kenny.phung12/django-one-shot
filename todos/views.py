from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def show_list(request):
    list = TodoList.objects.all()
    context = {
        "show_list": list,
    }
    return render(request, "todos/todo_lists/lists.html", context)


def list_details(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "details_object": details,
    }
    return render(request, "todos/todo_lists/details.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/todo_lists/create.html", context)


def update_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        todolist = get_object_or_404(TodoList, id=id)
        form = TodoListForm(instance=todolist)
    context = {
        "form": form
    }
    return render(request, "todos/todo_lists/update.html", context)


def delete_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_lists/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/todo_items/create.html", context)


def update_todo_item(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "form": form
    }
    return render(request, "todos/todo_items/update.html", context)
